﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YourTwitter.Models
{
    public class WeatherInfo
    {
        public Coord coord { get; set; }
        public List<Weather> weather { get; set; }
        public main main { get; set; }
        public int visibility { get; set; }
        public wind wind { get; set; }
        public cloud cloud { get; set; }
        public sys sys { get; set; }
        public int timezone { get; set; }
        public string name { get; set; }
        public int cod { get; set; }
    }

    public class Coord
    {
        public float lon { get; set; }
        public float lat { get; set; }
    }

    public class Weather
    {
        public int id { get; set; }
        public string main { get; set; }
        public string description { get; set; }
        public string icon { get; set; }
    }
    
    public class main
    {
        public float temp { get; set; }
        public float feels_like { get; set; }
        public float temp_min { get; set; }
        public float temp_max { get; set; }
        public int pressure { get; set; }
        public int humidity { get; set; }
    }

    public class wind
    {
        public float speed { get; set; }
        public int deg { get; set; }
        public float gust { get; set; }
    }

    public class sys
    {
        public string country { get; set; }
    }
    public class cloud
    {
        public int all { get; set; }
    }
}