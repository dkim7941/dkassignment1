﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using YourTwitter.Models;

namespace YourTwitter.Controllers
{
    public class HomeController : Controller
    {
        private const string WEATHER_APPID = "bf32061352e6df02411a3226ec255c0f";
        private const int TWEETS_COUNT = 20;

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            string ip = GetPublicIP();
            LocationInfo location = GetLocationFromIP(ip);
            WeatherInfo weather = GetWeather(location.city, location.countryCode);
            ViewData["imageUrl"] = string.Format("http://openweathermap.org/images/flags/{0}.png", location.countryCode.ToLower());
            ViewData["weatherIcon"] = string.Format("http://openweathermap.org/img/w/{0}.png", weather.weather[0].icon);
            ViewData["info"] = $"{location.city}, {location.countryCode} - Description: {weather.weather[0].description}";
            ViewData["temperature"] = $"Temperature: {Math.Round(weather.main.temp - 273.15, 0)}C " +
                $"- Feels like: {Math.Round(weather.main.feels_like - 273.15, 0)}C " +
                $"- Min: {Math.Round(weather.main.temp_min - 273.15, 0)}C " +
                $"- Max: {Math.Round(weather.main.temp_max - 273.15, 0)}C";
            return View();
        }


        [Authorize]
        public IActionResult Feed()
        {
            //var accessToken = 
               //HttpContext.GetTokenAsync("access_token").ToString();

            string ip = GetPublicIP();
            LocationInfo location = GetLocationFromIP(ip);
            WeatherInfo weather = GetWeather(location.city, location.countryCode);
            TweetResult tweetResult = GetTweets(location.lat, location.lon);
            ViewData["imageUrl"] = string.Format("http://openweathermap.org/images/flags/{0}.png", location.countryCode.ToLower());
            ViewData["weatherIcon"] = string.Format("http://openweathermap.org/img/w/{0}.png", weather.weather[0].icon);
            ViewData["info"] = $"{location.city}, {location.countryCode} - Description: {weather.weather[0].description}";
            ViewData["temperature"] = $"Temperature: {Math.Round(weather.main.temp - 273.15, 0)}C " +
                $"- Feels like: {Math.Round(weather.main.feels_like - 273.15, 0)}C " +
                $"- Min: {Math.Round(weather.main.temp_min - 273.15, 0)}C " +
                $"- Max: {Math.Round(weather.main.temp_max - 273.15, 0)}C";
            ViewData["tweetResult"] = tweetResult;
            return View(weather);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private string GetPublicIP()
        {
            return new WebClient().DownloadString("http://icanhazip.com");
        }

        private LocationInfo GetLocationFromIP(string ip)
        {
            string url = String.Format("http://ip-api.com/json/{0}", ip);
            using (WebClient client = new WebClient())
            {
                string json = client.DownloadString(url);
                LocationInfo location = JsonConvert.DeserializeObject<LocationInfo>(json);
                return location;
            }
        }


        private WeatherInfo GetWeather(string city, string countryCode)
        {
            string url = String.Format("http://api.openweathermap.org/data/2.5/weather?q={0},{1}&appid={2}", city, countryCode, WEATHER_APPID);
            using (WebClient client = new WebClient())
            {
                string json = client.DownloadString(url);
                WeatherInfo weather = JsonConvert.DeserializeObject<WeatherInfo>(json);
                return weather;
            }
        }

        private TweetResult GetTweets(float lat, float lon, int count = TWEETS_COUNT)
        {
            string url = String.Format("https://api.twitter.com/1.1/search/tweets.json?q=&geocode={0},{1},50km&result_type=recent&count={2}", lat, lon, count);
            string consumer_key = "7xUyd22UoVm4KQmjhCh2FzymI";
            string consumer_secret = "fEjv0Qt5BeIfoViNAJujVVoosUj7Rxh26jIXRXTOC4paCr9ydE";
            string key = Base64Helper.encode(consumer_key + ":" + consumer_secret);

            using (WebClient client = new WebClient())
            {
                //string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(consumer_key + ":" + consumer_secret));
                //client.Headers[HttpRequestHeader.Authorization] = "Basic " + credentials;
                string credentials = "AAAAAAAAAAAAAAAAAAAAAMjpKQEAAAAAdY%2Bnz0CgOAwVNfRKnexLu%2Fx5zKY%3D0RqAYZjo6njELi5R3MkQRttcKFIETd5jHxaRpqRcEsBZOPkwm3";
                client.Headers[HttpRequestHeader.Authorization] = "Bearer " + credentials;
                string json = client.DownloadString(url);
                TweetResult tweet = JsonConvert.DeserializeObject<TweetResult>(json);
                return tweet;
            }
        }
    }
}
