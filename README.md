Welcome to YourTwitter

## Installation

You’ll start by publishing the project from Visual Studio.

1. Find the folder *source code/bin/netcoreapp3.1/Release/publish*.
2. If you have it, skip to #4, otherwise, open the project in Visual Studio.
3. Select **Build** menu – **Publish YourTwitter** and click Publish to create the publish folder.
4. Copy the publish folder to your desired folder to use as the root of your web server. Rename the folder as you wish.

---

## Accessing the web site with a browser

Next, you’ll add a new file to this repository.

1. On your local machine, you can open http://localhost:(port).The default port number is 80 for http and 443 for https unless you have set a different port number for the website.
2. In order to make it public to the Internet, you need to have Port Forwarding set up on your router. Follow your router’s manual to set port forwarding rules, as the router settings UI varies on routers’ brands and models.

---

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details